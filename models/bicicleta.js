class Bicicleta {
    constructor (id, modelo, color, coordenadas){
        this.id = id;
        this.modelo = modelo;
        this.color = color;
        this.coordenadas = coordenadas;
    }
}

module.exports = Bicicleta;