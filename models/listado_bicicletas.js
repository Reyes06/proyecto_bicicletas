let Bicicleta = require('./bicicleta');

let b1 = new Bicicleta(123, 'Hyuga', 'Azul', [14.611585, -90.656519]);
let b2 = new Bicicleta(512, 'Uchiha', 'Rojo', [14.610670, -90.656444]);
let b3 = new Bicicleta(443,'Senju', 'Cafe', [14.613317, -90.658064]);

module.exports = [b1,b2,b3];